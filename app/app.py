"""VID mock."""
from typing import NoReturn
from flask import Flask
from flask_restful import Api, Resource


class VidResource(Resource):
    """Dummy resource."""

    def post(self) -> NoReturn:
        """Do simply nothing."""


app = Flask(__name__)
api = Api(app)


api.add_resource(
    VidResource,
    "/vid/maintenance/category_parameter/owningEntity",
    "/vid/maintenance/category_parameter/project",
    "/vid/maintenance/category_parameter/lineOfBusiness",
    "/vid/maintenance/category_parameter/platform",
)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
